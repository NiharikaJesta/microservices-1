package com.example.Consumer.App.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerController {
   @Autowired
   RestTemplate restTemplate;
//its a class which can be used to call any external web service
   @RequestMapping(value = "/Consume")
   public String getProductList() {
      HttpHeaders headers = new HttpHeaders();
	  headers.setBearerAuth("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyMyIsImV4cCI6MTY2ODQyMDk3MCwiaWF0IjoxNjY4NDAyOTcwfQ.afOnoBakPDo0lvEq6gaSmy0HMCEvR710Etn6PrYP7Hq75JWtAkU8HwXTsYnb7I_YJsYiVotHwUdkVF3ROMPp9g");
     // headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
      HttpEntity <String> entity = new HttpEntity<String>(headers);
      
      return restTemplate.exchange("http://localhost:8086/hello", HttpMethod.GET, entity, String.class).getBody();
   }
}

