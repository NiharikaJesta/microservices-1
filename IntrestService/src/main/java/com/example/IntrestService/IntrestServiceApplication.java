package com.example.IntrestService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
@EnableDiscoveryClient
@SpringBootApplication
public class IntrestServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntrestServiceApplication.class, args);
	}

}
